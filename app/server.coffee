# read config from env 
@location = process.env.CIMON_LOCATION
if not @location
  console.log "environment variable CIMON_LOCATION missing"
  process.exit(1)

@serverUrl = process.env.CIMON_MQTT_SERVER
if not @serverUrl
  console.log "environment variable CIMON_MQTT_SERVER missing"
  process.exit(1)

@serverPort = process.env.CIMON_MQTT_PORT
if not @serverUrl
  console.log "environment variable CIMON_MQTT_PORT missing"
  process.exit(1)

@pushIntervall = process.env.CIMON_PUSH_INTERVALL
if not @pushIntervall
  console.log "environment variable CIMON_PUSH_INTERVALL missing"
  process.exit(1)

@mqttuser = process.env.CIMON_MQTT_USER
if not @mqttuser
  console.log "environment variable CIMON_MQTT_USER missing"
  process.exit(1)

@mqttpass = process.env.CIMON_MQTT_PASS
if not @mqttuser
  console.log "environment variable CIMON_MQTT_PASS missing"
  process.exit(1)




# usb client (https://github.com/node-hid/node-hid)
HID = require('node-hid')
# mqtt client (https://github.com/adamvr/MQTT.js)
mqtt = require('mqtt')
settings = {
  username: @mqttuser,
  password: @mqttpass,
  keepalive: 1000,
  port: @serverPort,
  protocolId: 'MQTT',
  protocolVersion: 4,
  clientId: "cimon@#{@location}"
}
mqtt = mqtt.connect(@serverUrl, settings)


# usb-bridge (https://github.com/node-hid/node-hid)
findDevice = () ->
  devices = HID.devices()
  for device in devices
    if device['vendorId'] is 3141 and device['interface'] is 1
      console.log "using device #{JSON.stringify(device)}"
      return new HID.HID(device['path'])

# stolen from node-temper (https://github.com/asmuelle/node-temper1)
renderData = (hiByte, loByte) ->
  if ((hiByte & 128) == 128)
    return -((256-hiByte) + (1 + ~(loByte >> 4)) / 16.0)
  else
    return hiByte + ((loByte >> 4) / 16.0)

# trigger onData (registered with the device) by writing a read command to the device
readTemperature = (device) ->
  device.write [0x01, 0x80, 0x33, 0x01, 0x00, 0x00, 0x00, 0x00]
  #console.log 'wrote read command to USB device'


# callback for the usb-data returned after read command
# just refreshes @currentTemperature
onData = (data) =>
  msg = {
      "location": {
          "name": @location
        },
      "temperature":{
        "value":renderData data[2], data[3]
      },
      "time": new Date().toString()
    }
  @currentTemperature = JSON.stringify(msg)

# find device
device = findDevice(onData)
# register onData handler
if device
  device.on "data", onData
else
  console.log "fatal - no thermometer found. exiting."
  process.exit(1)
# send temperature-message to 123k.de
publishTemperature = (mqtt) =>
  if @currentTemperature
    #console.log "publishing temperature #{@currentTemperature}"
    mqtt.publish 'temp-stream', @currentTemperature
  else
    console.log "no temperature available to publish"

# schedule events
setInterval readTemperature, 10 * 1000, device
setInterval publishTemperature, @pushIntervall * 1000, mqtt

console.log "~> cimon booted at:'#{new Date()}'. working for #{@location}"
