# Welcome to _cimon_
_cimon_ is an node.js app which acts an event-source for my thermometer view at [123k.work](http://123k.work). It periodically reads the temperature from a simple USB thermometer and remembers that value. Other jobs come along an push this value via MQTT to the frontend application [teger](https://github.org/efwe/teger).

#Have any suggestions? 
Tell me about it on Twitter - [@\_efwe\_](https://twitter.com/_efwe_).
